[![License: CC0-1.0](https://img.shields.io/badge/License-CC0_1.0-lightgrey.svg)](http://creativecommons.org/publicdomain/zero/1.0/)

# TRANCE
Transmission Recieving Artnet Network Conversions Encoder. This is a Open Source and Hardware Device that conversts s.ACN over wifi to DMX.

This device is finished, and a Prototype already can be purchased. Documentation follows.
[Buy TRANCE](https://3d-mueller.mycommerce.shop/products/Trance-p678585327)

## Roadmap
- [ ] Create BOM
- [ ] Add Artnet support
- [ ] Internet Settings via Webinterface
- [ ] Collect License Files
- [ ] Write Small manual in Readme